/*
** data_transfers.c for ftp in /home/godard_b/workspace/Projets/En_cours/my_ftp/rendu/server_src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Sun Apr  7 17:31:59 2013 tuatini godard
** Last update Sat Apr 13 17:39:55 2013 tuatini godard
*/

#include "my_ftp.h"
#include "server.h"

int			create_data_connection(t_attr *attr,
			       t_message *msg, int text_socket)
{
  struct sockaddr_in	servaddr;

  set_message_reply(msg, REPL_425);
  if (attr->ft_sock == -1)
    {
      if (attr->ft_port < 0 || attr->ft_port > 65535)
	  return (-1);
      servaddr.sin_family = AF_INET;
      servaddr.sin_addr.s_addr = inet_addr(attr->ft_ip);
      servaddr.sin_port = htons (attr->ft_port);
      if ((attr->ft_sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
	  return (-1);
      if (connect(attr->ft_sock, (struct sockaddr *) &servaddr,
		  sizeof (servaddr)) == -1)
	{
	  attr->ft_sock = -1;
	  return (-1);
	}
      set_message_reply(msg, REPL_200);
      return (1);
    }
  return (-1);
}
