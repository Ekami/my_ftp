/*
** user_commands4.c for ftp in /home/godard_b/workspace/Projets/En_cours/my_ftp/rendu/server_src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Sun Apr  7 21:27:19 2013 tuatini godard
** Last update Sat Apr 13 19:29:14 2013 tuatini godard
*/

#include "my_ftp.h"
#include "server.h"

long		send_dir_list(t_attr *attr, char *path)
{
  long		total;
  DIR		*dp;
  struct dirent	*ep;
  struct stat	s_buff;
  char		buffer[1024];

  total = 0;
  dp = opendir (path);
  if (dp != NULL)
      while (ep = readdir (dp))
	if (!is_special_dir(ep->d_name))
	  if (stat(ep->d_name, &s_buff) == 0)
	    {
	      memset(&buffer, 0, 1024);
	      get_file_infos(attr, ep->d_name, buffer, &s_buff);
	      total += strlen(buffer);
	      if (write(attr->ft_sock, buffer, strlen(buffer)) == -1)
		{
		  fprintf(stderr, "Can't write on socket!\n");
		  return (-1);
		}
	    }
  closedir (dp);
  return (total);
}

int	check_list(t_attr *attr, t_message *msg, t_command *cmd, int socket)
{
  long	ret;
  char	reply[1024];
  char	old_cwd[1024];
  char	*listPath;

  memset(reply, 0, 1024);
  if ((listPath = check_dir(attr, cmd->data)) != NULL)
    {
      set_message_reply(msg, REPL_150);
      getcwd(old_cwd, 1024);
      chdir(listPath);
      ret = send_dir_list(attr, listPath);
      if (ret != -1)
	sprintf(reply, REPL_150, ret);
      else
	sprintf(reply, REPL_150, 0);
      chdir(old_cwd);
      set_message_reply(msg, reply);
      if ((send_msg(socket, msg)) == -1)
	fprintf(stderr, "Can't write on socket!\n");
    }
  close(attr->ft_sock);
  attr->ft_sock = -1;
  return (ret);
}

int		list_cmd(t_attr *attr, t_command *cmd, int socket)
{
  t_message	msg;

  memset(&msg, 0, sizeof(t_message));
  printf("\ncmd=%s;data=%s;\n", cmd->command_code, cmd->data);
  if (create_data_connection(attr, &msg, socket) == -1)
    {
      set_message_reply(&msg, REPL_451);
      if ((send_msg(socket, &msg)) == -1)
	fprintf(stderr, "Can't write on socket!\n");
      set_message_reply(&msg, REPL_226);
      return (-1);
    }
  if (attr->ft_sock != -1)
    check_list(attr, &msg, cmd, socket);
  set_message_reply(&msg, REPL_226);
  if ((send_msg(socket, &msg)) == -1)
    fprintf(stderr, "Can't write on socket!\n");
  return (1);
}

int		cd_cmd(t_attr *attr, t_command *cmd, int socket)
{
  t_message	msg;
  char		*path;

  memset(&msg, 0, sizeof(t_message));
  set_message_reply(&msg, REPL_550);
  path = check_dir(attr, cmd->data);
  printf("data_cmd = %s\n", cmd->data);
  if (path != NULL)
    {
      chdir(path);
      set_message_reply(&msg, REPL_250);
    }
  if ((send_msg(socket, &msg)) == -1)
    fprintf(stderr, "Can't write on socket!\n");
  return (1);
}
