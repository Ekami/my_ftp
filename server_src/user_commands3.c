/*
** user_commands3.c for ftp in /home/godard_b/workspace/Projets/En_cours/my_ftp/rendu/server_src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Sun Apr  7 14:33:51 2013 tuatini godard
** Last update Sat Apr 13 20:55:13 2013 tuatini godard
*/

#include "my_ftp.h"
#include "server.h"

static int	parse_port_data(t_attr *attr, char *data_buff)
{
  int		len;
  int		port[2];
  char		*next_tok;

  len = 0;
  data_buff[strlen(data_buff) - 1] = 0;
  next_tok = strtok(data_buff, ",");
  if (next_tok == NULL || atoi(next_tok) < 0 || atoi(next_tok) >= 255)
    return (-1);
  port[0] = atoi(next_tok);
  len += strlen(next_tok) + 1;
  if (atoi(data_buff + len) < 0 || atoi(data_buff + len) >= 255)
    return (-1);
  port[1] = atoi(data_buff + len);
  attr->ft_port = port[0] * 256 + port[1];
  return (1);
}

static int	parse_addr_data(t_attr *attr, char *data_buff)
{
  char		*next_tok;
  int		len;
  int		i;

  i = 0;
  len = 0;
  while (i < 4)
    {
      next_tok = strtok(data_buff + len, ",");
      if (next_tok == NULL || atoi(next_tok) < 0 || atoi(next_tok) >= 255)
	return (-1);
      strncpy(attr->ft_ip + len, next_tok, strlen(next_tok));
      len += strlen(next_tok);
      strcpy(attr->ft_ip + len++, ".");
      i++;
    }
  strcpy(attr->ft_ip + len - 1, "\0");
  return (parse_port_data(attr, data_buff + len));
}

int		port_cmd(t_attr *attr, t_command *cmd, int socket)
{
  t_message	msg;

  memset(&msg, 0, sizeof(t_message));
  if (parse_addr_data(attr, cmd->data) == -1)
    {
      strcpy(attr->ft_ip, "127.0.0.1");
      attr->ft_port = 60423;
      set_message_reply(&msg, REPL_501);
    }
  else
    set_message_reply(&msg, REPL_200);
  if ((send_msg(socket, &msg)) == -1)
    fprintf(stderr, "Can't write on socket!\n");
  return (1);
}
