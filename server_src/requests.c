/*
** requests.c for ftp in /home/godard_b/workspace/Projets/En_cours/my_ftp/rendu/server_src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Tue Apr  2 03:08:56 2013 tuatini godard
** Last update Sun Apr 14 01:36:56 2013 tuatini godard
*/

#include "my_ftp.h"
#include "server.h"

void	client_cmds_work(t_attr *attr, t_command *cmd,
			 int client_fd, t_command_ptr *commands)
{
  int		i;
  int		command_exists;
  int		commands_lenght;

  i = 0;
  commands_lenght = 10;
  command_exists = 0;
  while (i < commands_lenght)
    {
      if (cmp_codes(cmd->command_code,
		    commands[i].cmd_code, strlen(commands[i].cmd_code)))
	{
	  my_putstr(cmd->command_code);
	  my_putstr(" command received \n");
	  commands[i].exec_cmd(attr, cmd, client_fd);
	  i = commands_lenght;
	  command_exists = 1;
	}
      i++;
    }
  if (!command_exists)
    unkown_cmd(attr, cmd, client_fd);
}

void		interpret_client_cmds(t_attr *attr, t_command *cmd, int client_fd)
{
  t_command_ptr commands[]= {
    {&check_username, CMD_USER},
    {&check_password, CMD_PASS},
    {&syst_cmd, CMD_SYST},
    {&pwd_cmd, CMD_PWD},
    {&cd_cmd, CMD_CWD},
    {&type_cmd, CMD_TYPE},
    {&pasv_cmd, CMD_PASV},
    {&port_cmd, CMD_PORT},
    {&list_cmd, CMD_LIST},
    {&quit_cmd, CMD_QUIT}
  };
  client_cmds_work(attr, cmd, client_fd, commands);
}

int		initialize_session(t_attr *attr, int client_fd)
{
  t_message	msg;
  t_command	response;

  memset(&msg, 0, sizeof(t_message));
  set_message_reply(&msg, REPL_220);
  if ((send_msg(client_fd, &msg)) != -1)
    printf("Client %d session initialized!\n", client_fd);
  memset(attr->initial_dir, 0, 1024);
  getcwd(attr->initial_dir, 1024);
  strcpy(attr->ft_ip, "127.0.0.1");
  attr->ft_port = 60423;
  attr->ft_sock = -1;
  attr->transf_type = IMAGE_BINARY;
  return (1);
}

void		wait_user_inputs(int client_fd)
{
  t_command	cmd;
  t_attr	attributes;

  initialize_session(&attributes, client_fd);
  while (1)
    {
      memset(&cmd, 0, sizeof(t_command));
      if ((receive_cmd(client_fd, &cmd)) != -1)
	interpret_client_cmds(&attributes, &cmd, client_fd);
    }
}
