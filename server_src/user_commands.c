/*
** user_commands.c for ftp in /home/godard_b/workspace/Projets/En_cours/my_ftp/rendu/server_src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Fri Apr  5 05:03:39 2013 tuatini godard
** Last update Sat Apr 13 02:45:41 2013 tuatini godard
*/

#include "my_ftp.h"
#include "server.h"

int		check_username(t_attr *attr, t_command *cmd, int socket)
{
  t_message	msg;

  memset(&msg, 0, sizeof(t_message));
  if (strncmp(cmd->data, "anonymous", strlen("anonymous")) == 0)
    {
      set_message_reply(&msg, REPL_331);
      if ((send_msg(socket, &msg)) == -1)
	fprintf(stderr, "Client username incorrect!\n");
      return (-1);
    }
  else
    {
      set_message_reply(&msg, REPL_430);
      write(1, "Wrong username supplied\n", 24);
      if ((send_msg(socket, &msg)) == -1)
	fprintf(stderr, "Client username incorrect!\n");
      return (-1);
    }
  return (1);
}

int		check_password(t_attr *attr, t_command *cmd, int socket)
{
  t_message	msg;

  memset(&msg, 0, sizeof(t_message));
  if (strncmp(cmd->data, "anonymous", strlen("anonymous")) == 0)
    {
      set_message_reply(&msg, REPL_230);
      if ((send_msg(socket, &msg)) == -1)
	fprintf(stderr, "Client password incorrect!\n");
      return (-1);
    }
  else
    {
      set_message_reply(&msg, REPL_430);
      write(1, "Wrong password supplied\n", 24);
      if ((send_msg(socket, &msg)) == -1)
	fprintf(stderr, "Client password incorrect!\n");
      return (-1);
    }
  return (1);
}

int		syst_cmd(t_attr *attr, t_command *cmd, int socket)
{
  t_message	msg;

  memset(&msg, 0, sizeof(t_message));
  set_message_reply(&msg, REPL_215);
  if ((send_msg(socket, &msg)) == -1)
    fprintf(stderr, "Can't write on socket!\n");
  return (1);
}

int		unkown_cmd(t_attr *attr, t_command *cmd, int socket)
{
  t_message	msg;

  memset(&msg, 0, sizeof(t_message));
  set_message_reply(&msg, REPL_500);
  my_putstr("Unknown cmd: ");
  my_putstr(cmd->command_code);
  my_putstr("\n");
  if ((send_msg(socket, &msg)) == -1)
    fprintf(stderr, "Can't write on socket!\n");
  return (1);
}
