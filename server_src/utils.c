/*
** utils.c for ftp in /home/godard_b/workspace/Projets/En_cours/my_ftp/rendu/server_src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Mon Apr  8 00:28:21 2013 tuatini godard
** Last update Mon Apr  8 04:37:08 2013 tuatini godard
*/

#include "my_ftp.h"
#include "server.h"

/*
 * Check if the given directory is none of ".", or ".."
 */
int	is_special_dir(char *dir)
{
  int	len;

  if(dir == NULL)
    return (1);
  len = strlen(dir);
  if(len > 2)
    return (0);
  if(dir[0] != '.')
    return (0);
  if(len == 1)
    return (1);
  if(dir[1] == '.')
    return (1);
  return (0);
}

void	append(char *s, char c)
{
  int	len;

  len = strlen(s);
  s[len] = c;
  s[len + 1] = '\0';
}
