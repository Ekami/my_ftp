/*
** main.c for ftp in /home/godard_b/workspace/Projets/En_cours/my_ftp/rendu
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Mon Apr  1 16:32:19 2013 tuatini godard
** Last update Mon Apr  1 21:38:16 2013 tuatini godard
*/

#include "my_ftp.h"
#include "server.h"

int	get_port(int ac, char **av)
{
  int	port;

  if (ac < 2 || ac > 2)
    {
      printf("Usage : ./serveur port");
      return (-1);
    }
  port = atoi(av[1]);
  return (port);
}

int	main(int ac, char **av)
{
  int	port;

  if ((port = get_port(ac, av)) != -1)
    start_server(port);
  printf("\n");
  return (0);
}
