/*
** user_commands2.c for ftp in /home/godard_b/workspace/Projets/En_cours/my_ftp/rendu/server_src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Fri Apr  5 23:12:06 2013 tuatini godard
** Last update Sat Apr 13 14:34:50 2013 tuatini godard
*/

#include "my_ftp.h"
#include "server.h"

char	*generate_root_cwd(t_attr *attr, char *path)
{
  char	*ret;

  ret = malloc(sizeof(char) * FTP_CMD_DATA_SIZE);
  if (ret == NULL)
    {
      write(2, "Server out of memory\n", 21);
      return (NULL);
    }
  memset(ret, 0, FTP_CMD_DATA_SIZE);
  if (strlen(attr->initial_dir) == strlen(path))
    ret[0] = '/';
  else if (strlen(attr->initial_dir) > strlen(path))
    return (NULL);
  else
    strcpy(ret, path + strlen(attr->initial_dir));
  return (ret);
}

int		pwd_cmd(t_attr *attr, t_command *cmd, int socket)
{
  char		reply[FTP_CMD_DATA_SIZE];
  t_message	msg;
  char		*cwd;

  cwd = generate_root_cwd(attr, getcwd(NULL, 1024));
  if (cwd != NULL)
    sprintf(reply, REPL_257_PWD, cwd);
  else
    {
      set_message_reply(&msg, REPL_426);
      if ((send_msg(socket, &msg)) == -1)
	fprintf(stderr, "Can't write on socket!\n");
      return (-1);
    }
  memset(&msg, 0, sizeof(t_message));
  set_message_reply(&msg, reply);
  if ((send_msg(socket, &msg)) == -1)
    fprintf(stderr, "Can't write on socket!\n");
  return (1);
}

int		type_cmd(t_attr *attr, t_command *cmd, int socket)
{
  t_message	msg;

  if (strncmp(cmd->data, "A", 1) == 0)
    attr->transf_type = ASCII_TEXT;
  else if (strncmp(cmd->data, "E", 1) == 0)
    attr->transf_type = EBCDIC_TEXT;
  else if (strncmp(cmd->data, "L", 1) == 0)
    attr->transf_type = LOCAL_FORMAT;
  else
    attr->transf_type = IMAGE_BINARY;
  memset(&msg, 0, sizeof(t_message));
  set_message_reply(&msg, REPL_200);
  if ((send_msg(socket, &msg)) == -1)
    fprintf(stderr, "Can't write on socket!\n");
  return (1);
}

int		pasv_cmd(t_attr *attr, t_command *cmd, int socket)
{
  t_message	msg;

  memset(&msg, 0, sizeof(t_message));
  set_message_reply(&msg, REPL_502);
  if ((send_msg(socket, &msg)) == -1)
    fprintf(stderr, "Can't write on socket!\n");
  return (1);
}

int		quit_cmd(t_attr *attr, t_command *cmd, int socket)
{
  t_message	msg;

  memset(&msg, 0, sizeof(t_message));
  set_message_reply(&msg, REPL_221);
  if ((send_msg(socket, &msg)) == -1)
    fprintf(stderr, "Can't write on socket!\n");
  if (socket != -1)
    {
      close(socket);
      my_putstr("Connection with a client just terminated!\n");
      exit(0);
    }
  return (1);
}
