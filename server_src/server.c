/*
** server.c for ftp in /home/godard_b/workspace/Projets/En_cours/my_ftp/rendu/server_src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Mon Apr  1 17:33:33 2013 tuatini godard
** Last update Sun Apr 14 01:41:13 2013 tuatini godard
*/

#include "my_ftp.h"
#include "server.h"

int	clients_fork(int socket_fd)
{
  int	i;
  int	client_fd;
  pid_t	pid;

  i = 0;
  while (i < MAX_CLIENTS)
    {
      client_fd = accept(socket_fd, (struct sockaddr *) NULL, NULL);
      printf("New client with fd: %d connected\n", client_fd);
      if ((pid = fork()) == -1)
	return (write(2, strerror(errno), strlen(strerror(errno))));
      if (pid == 0)
	  wait_user_inputs(client_fd);
      i++;
    }
  return (0);
}

int			start_server(int port)
{
  int			socket_fd;
  int			bind_ret;
  int			listen_ret;
  struct sockaddr_in	server_addr;

  socket_fd = socket(AF_INET, SOCK_STREAM, 0);
  if (socket_fd == -1)
    return (write(2, "ERROR opening socket\n", 21));
  memset(&server_addr, '0', sizeof(server_addr));
  server_addr.sin_family = AF_INET;
  server_addr.sin_addr.s_addr = INADDR_ANY;
  server_addr.sin_port = htons(port);
  bind_ret = bind(socket_fd, (struct sockaddr *) &server_addr,
		  sizeof(server_addr));
  listen_ret = listen(socket_fd, MAX_CLIENTS);
  if (bind_ret == -1 || listen_ret == -1)
    return (write(2, strerror(errno), strlen(strerror(errno))));
  return (clients_fork(socket_fd));
}
