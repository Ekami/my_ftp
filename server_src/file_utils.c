/*
** file_utils.c for ftp in /home/godard_b/workspace/Projets/En_cours/my_ftp/rendu/server_src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Mon Apr  8 05:14:05 2013 tuatini godard
** Last update Sat Apr 13 15:31:31 2013 tuatini godard
*/

#include "my_ftp.h"
#include "server.h"

void	set_modes(char *mode, struct stat *s_buff)
{
  mode[1] = (s_buff->st_mode & S_IRUSR)?'r':'-';
  mode[2] = (s_buff->st_mode & S_IWUSR)?'w':'-';
  mode[3] = (s_buff->st_mode & S_IXUSR)?'x':'-';
  mode[4] = (s_buff->st_mode & S_IRGRP)?'r':'-';
  mode[5] = (s_buff->st_mode & S_IWGRP)?'w':'-';
  mode[6] = (s_buff->st_mode & S_IXGRP)?'x':'-';
  mode[7] = (s_buff->st_mode & S_IROTH)?'r':'-';
  mode[8] = (s_buff->st_mode & S_IWOTH)?'w':'-';
  mode[9] = (s_buff->st_mode & S_IXOTH)?'x':'-';
}

int		get_file_infos(t_attr *attr, char *filename,
		       char *buffer, struct stat *s_buff)
{
  char		date[16];
  char		mode[11];
  int		b_mask;
  struct group	*group_info;
  struct passwd	*pass_info;

  strcpy(mode, "----------");
  group_info = getgrgid(s_buff->st_gid);
  pass_info = getpwuid(s_buff->st_uid);
  if (pass_info != NULL)
    {
      if (group_info != NULL)
	{
	  b_mask = s_buff->st_mode & S_IFMT;
	  mode[0] = b_mask == S_IFDIR ? 'd' : b_mask == S_IFREG ? '-' : -1;
	  if (mode[0] == -1)
	    return (-1);
	  set_modes(mode, s_buff);
	  strftime(date, 13, "%b %d %H:%M", localtime(&(s_buff->st_mtime)));
	  sprintf(buffer,"%s %3d %-4s %-4s %8d %12s %s\r\n",
		  mode, s_buff->st_nlink, pass_info->pw_name, group_info->gr_name,
		  s_buff->st_size, date, filename);
	}
    }
  return (1);
}


int	check_dir_end(t_attr *attr, char *tmp, char *new_cwd, char *old_cwd)
{
  tmp[strlen(tmp) - 1] = 0;
  if (chdir(tmp) == -1)
    return (-1);
  getcwd(new_cwd, 1024);
  chdir(old_cwd);
  if (strlen(new_cwd) < strlen(attr->initial_dir))
    return (-1);
  return (1);
}

char	*check_dir(t_attr *attr, char *path)
{
  char	*tmp;
  char	new_cwd[1024];
  char	old_cwd[1024];

  if ((tmp = malloc(sizeof(char) * 1024)) == NULL ||
      path == NULL || attr == NULL)
    return (NULL);
  memset(tmp, 0, 1024);
  getcwd(old_cwd, 1024);
  if (path[0] == '/')
    {
      strcpy(tmp, attr->initial_dir);
      strcat(tmp, path);
    }
  else
    {
      strcpy(tmp, old_cwd);
      strcat(tmp, "/");
      strcat(tmp, path);
    }
  if (check_dir_end(attr, tmp, new_cwd, old_cwd) == -1)
    return (NULL);
  return (tmp);
}
