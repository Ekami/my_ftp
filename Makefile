##
## Makefile for Makefile in /home/godard_b//workspace/epitech/
## 
## Made by tuatini godard
## Login   <godard_b@epitech.net>
## 
## Started on  Mon Oct 22 12:05:00 2012 tuatini godard
## Last update Sat Apr 13 03:17:20 2013 tuatini godard
##

all:
	make -C client_src/
	make -C server_src/
	cp client_src/client .
	cp server_src/serveur .

client:
	make -C client_src/
	cp client_src/client .

serveur:
	make -C server_src/
	cp server_src/serveur .

re:     fclean all

fclean:
	make fclean -C client_src/
	make fclean -C server_src/
	rm -f client
	rm -f serveur

clean:
	make clean -C client_src/
	make clean -C server_src/
