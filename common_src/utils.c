/*
** utils.c for ftp in /home/godard_b/workspace/Projets/En_cours/my_ftp/rendu/server_src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Tue Apr  9 00:50:38 2013 tuatini godard
** Last update Tue Apr  9 00:55:17 2013 tuatini godard
*/

#include "my_ftp.h"

int	my_putchar(char c)
{
  return (write(1, &c, 1));
}

int	my_putstr(char *str)
{
  write(1, str, strlen(str));
}
