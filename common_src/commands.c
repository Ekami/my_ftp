/*
** commands.c for ftp in /home/godard_b/workspace/Projets/En_cours/my_ftp/rendu/server_src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Fri Apr  5 20:57:19 2013 tuatini godard
** Last update Sun Apr 14 02:06:51 2013 tuatini godard
*/

#include "my_ftp.h"

int	serialize_cmd(t_command *cmd, char *a_result)
{
  if(cmd == NULL)
    return (-1);
  strcpy(a_result, cmd->command_code);
  if (strlen(cmd->command_code) == 3)
    {
      strcpy(a_result + 3, " ");
      strcpy(a_result + 4, " ");
    }
  if (strlen(cmd->command_code) == 4)
    strcpy(a_result + 4, " ");
  strncpy(&a_result[FTP_CMD_CODE_SIZE], cmd->data, FTP_CMD_DATA_SIZE);
  if (cmd->data != NULL)
    strcat(a_result, "\r\n");
  return (0);
}

int	deserialize_cmd(char *a_str, t_command *msg_result)
{
  char	*tmp;

  if (a_str == NULL)
    return (-1);
  memset(msg_result, 0, sizeof(t_command));
  strncpy(msg_result->command_code, a_str, FTP_CMD_CODE_SIZE);
  strncpy(msg_result->data, a_str + FTP_CMD_CODE_SIZE, FTP_CMD_DATA_SIZE);
  if (msg_result->data[strlen(msg_result->data) - 1] == '\n' ||
      msg_result->data[strlen(msg_result->data) - 1] == '\t')
    msg_result->data[strlen(msg_result->data) - 1] = 0;
  return (0);
}

int		send_cmd(int socket, t_command *query)
{
  char		buff[FTP_T_CMD_SIZE];
  ssize_t	total;
  int		n;

  total = 0;
  n = 0;
  if(serialize_cmd(query, buff) == -1)
    {
      fprintf(stderr, "Error: Message serialization failed\n");
      return (-1);
    }
  while(total < FTP_T_CMD_SIZE)
    {
      if ((n = write(socket, buff + total, FTP_T_CMD_SIZE - total)) == -1)
	{
	  fprintf(stderr, "Error: Could not send the message\n");
	  return (-1);
	}
      total += n;
    }
  return (0);
}

int		receive_cmd(int a_socket, t_command *ap_response)
{
  char		buff[FTP_T_CMD_SIZE];
  ssize_t	total;
  int		n;

  total = 0;
  n = 0;
  memset(buff, '\0', FTP_T_CMD_SIZE);
  while(total < FTP_T_CMD_SIZE)
    {
      if((n = read(a_socket, buff + total, FTP_T_CMD_SIZE - total)) == -1)
	{
	  fprintf(stderr, "Error: Could not receive the message\n");
	  return (-1);
	}
      if (buff[total - 1] == 0)
	total = FTP_T_CMD_SIZE;
      total += n;
    }
  return (deserialize_cmd(buff, ap_response));
}
