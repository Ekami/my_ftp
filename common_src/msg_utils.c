/*
 ** checker.c for ftp in /home/godard_b/workspace/Projets/En_cours/my_ftp/rendu/server_src
 **
 ** Made by tuatini godard
 ** Login   <godard_b@epitech.net>
 **
 ** Started on  Tue Apr  2 15:32:21 2013 tuatini godard
** Last update Fri Apr 12 22:23:45 2013 tuatini godard
 */

#include "my_ftp.h"

int	cmp_codes(char *code1, char *code2, int size)
{
  int	ret;

  if (code1 == NULL || code2 == NULL)
    return (-1);
  ret = strncmp(code1, code2, size);
  if (ret == 0)
    return (1);
  return (0);
}

void	set_message_reply(t_message *t_msg, char *reply_code)
{
  set_msg_reply_code(t_msg, reply_code);
  set_msg(t_msg, reply_code + FTP_REPL_CODE_SIZE);
}

void	set_msg_reply_code(t_message *msg, char *code)
{
  strncpy(msg->reply_code, code, FTP_REPL_CODE_SIZE);
}

void	set_msg(t_message *t_msg, char *msg)
{
  strncpy(t_msg->msg, msg, FTP_MSG_SIZE);
}
