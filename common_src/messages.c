/*
 ** messages.c for ftp in /home/godard_b/workspace/Projets/En_cours/my_ftp/rendu/server_src
 **
 ** Made by tuatini godard
 ** Login   <godard_b@epitech.net>
 **
 ** Started on  Tue Apr  2 00:49:24 2013 tuatini godard
** Last update Mon Apr  8 03:16:29 2013 tuatini godard
 */

#include "my_ftp.h"

int	serialize_msg(t_message *msg, char *a_result)
{
  if(msg == NULL)
    return (-1);
  strncpy(a_result, msg->reply_code, FTP_REPL_CODE_SIZE);
  strncpy(&a_result[FTP_REPL_CODE_SIZE], msg->msg, FTP_MSG_SIZE);
  return (0);
}

int	deserialize_msg(char *a_str, t_message *msg_result)
{
  if(a_str == NULL)
    return (-1);
  strncpy(msg_result->reply_code, a_str, FTP_REPL_CODE_SIZE);
  msg_result->reply_code[FTP_REPL_CODE_SIZE] = '\0';
  strncpy(msg_result->msg, &a_str[FTP_REPL_CODE_SIZE], FTP_MSG_SIZE);
  return (0);
}

int		send_msg(int socket, t_message *query)
{
  char		buff[FTP_T_MSG_SIZE];
  ssize_t	total;
  int		n;

  n = 0;
  total = 0;
  if(serialize_msg(query, buff) == -1)
    {
      fprintf(stderr, "Error: Message serialization failed\n");
      return (-1);
    }
  while(total < FTP_T_MSG_SIZE)
    {
      if ((n = write(socket, buff + total, FTP_T_MSG_SIZE - total)) == -1)
	{
	  fprintf(stderr, "Error: Could not send the message\n");
	  return (-1);
	}
      total += n;
    }
  return (0);
}

int		receive_msg(int a_socket, t_message *ap_response)
{
  char		buff[FTP_T_MSG_SIZE];
  ssize_t	total;
  int		n;

  n = 0;
  total = 0;
  memset(buff, '\0', FTP_T_MSG_SIZE);
  while(total < FTP_T_MSG_SIZE)
    {
      if((n = read(a_socket, buff + total, FTP_T_MSG_SIZE - total)) == -1)
	{
	  fprintf(stderr, "Error: Could not receive the message\n");
	  return (-1);
	}
      if (buff[total - 1] == 0)
	total = FTP_T_MSG_SIZE;
      total += n;
    }
  return (deserialize_msg(buff, ap_response));
}
