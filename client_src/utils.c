/*
** utils.c for ftp in /home/godard_b/workspace/Projets/En_cours/my_ftp/rendu/client_src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Sat Apr 13 14:39:46 2013 tuatini godard
** Last update Sun Apr 14 02:10:12 2013 tuatini godard
*/

#include "my_ftp.h"
#include "client.h"

int	print_response(t_message *response)
{
  if (response->reply_code[0] == '4' ||
      response->reply_code[0] == '5' ||
      response->reply_code[0] == '6')
    {
      my_putstr("ERROR: ");
      my_putstr(response->reply_code);
      my_putstr(response->msg);
      if (response->msg[strlen(response->msg) - 1] != '\n')
	my_putstr("\n");
      return (-1);
    }
  else
    my_putstr("SUCCESS: ");
  my_putstr(response->reply_code);
  my_putstr(response->msg);
  if (response->msg[strlen(response->msg) - 1] != '\n')
    my_putstr("\n");
  return (1);
}

int			open_data_con(int port)
{
  int			socket_fd;
  int			bind_ret;
  int			listen_ret;
  struct sockaddr_in	server_addr;

  socket_fd = socket(AF_INET, SOCK_STREAM, 0);
  if (socket_fd == -1)
    return (-1);
  memset(&server_addr, '0', sizeof(server_addr));
  server_addr.sin_family = AF_INET;
  server_addr.sin_addr.s_addr = INADDR_ANY;
  server_addr.sin_port = htons(port);
  bind_ret = bind(socket_fd, (struct sockaddr *) &server_addr,
		  sizeof(server_addr));
  listen_ret = listen(socket_fd, 1);
  if (bind_ret == -1 || listen_ret == -1)
    return (-1);
  return (socket_fd);
}
