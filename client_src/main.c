/*
** main.c for ftp in /home/godard_b/workspace/Projets/En_cours/my_ftp/rendu
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Mon Apr  1 16:32:19 2013 tuatini godard
** Last update Tue Apr  9 03:36:25 2013 tuatini godard
*/

#include "my_ftp.h"
#include "client.h"

int	get_port(int ac, char **av)
{
  int	port;

  if (ac < 3 || ac > 3)
    {
      printf("Usage : ./client machine port");
      return (-1);
    }
  port = atoi(av[2]);
  return (port);
}

char	*get_addr(int ac, char **av)
{
  char	*addr;

  addr = av[1];
  return (addr);
}

int	main(int ac, char **av)
{
  int	port;
  char	*addr;

  port = get_port(ac, av);
  addr = get_addr(ac, av);
  if (port != -1)
    {
      if (launch_client(addr, port))
	{
	  my_putstr("\tERROR: The connection with the server was lost\n");
	  return (-1);
	}
    }
  printf("\n");
  return (0);
}
