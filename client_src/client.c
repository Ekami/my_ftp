/*
** client.c for ftp in /home/godard_b/workspace/Projets/En_cours/my_ftp/rendu/client_src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Mon Apr  1 20:43:38 2013 tuatini godard
** Last update Sun Apr 14 02:05:52 2013 tuatini godard
*/

#include "my_ftp.h"
#include "client.h"

void		identify_cmd(int socket_fd, char *cmd, char *param)
{
  int		i;
  int		cmd_found;
  t_command_ptr	commands[] = {
    {&ls_cmd, "ls"},
    {&port_cmd, "port"},
    {&pwd_cmd, "pwd"},
    {&cwd_cmd, "cd"},
    {&quit_cmd, "quit"}
  };

  i = 0;
  cmd_found = 0;
  while (i < 5)
    {
      if (strcmp(commands[i].cmd, cmd) == 0)
	{
	  commands[i].exec_cmd(socket_fd, cmd, param);
	  cmd_found = 1;
	}
      i++;
    }
  if (!cmd_found && strlen(cmd) != 0)
    my_putstr("ERROR: Command not found\n");
}

void		wait_user_command(int socket_fd)
{
  int		i;
  char		buffer[FTP_MSG_SIZE];
  char		cmd[FTP_MSG_SIZE];
  char		param[FTP_MSG_SIZE];

  my_putstr("ftp> ");
  while (1)
    {
      i = 0;
      memset(buffer, 0, FTP_MSG_SIZE);
      memset(cmd, 0, FTP_MSG_SIZE);
      memset(param, 0, FTP_MSG_SIZE);
      read(1, buffer, FTP_MSG_SIZE);
      while (i < FTP_MSG_SIZE)
	{
	  if (buffer[i] == ' ' || buffer[i] == '\t' || buffer[i] == '\n')
	    buffer[i] = 0;
	  i++;
	}
      strcpy(cmd, buffer);
      strcpy(param, buffer + strlen(cmd) + 1);
      identify_cmd(socket_fd, cmd, param);
      my_putstr("ftp> ");
    }
}

int		ask_password(int socket_fd)
{
  char		buffer[512];
  t_command	command;
  t_message	response;

  memset(&command, 0, sizeof(t_command));
  memset(&response, 0, sizeof(t_message));
  memset(&buffer, 0, 512);
  my_putstr("Password: ");
  read(1, buffer, 512);
  strcpy(command.command_code, CMD_PASS);
  strcpy(command.data, buffer);
  send_cmd(socket_fd, &command);
  if (receive_msg(socket_fd, &response) == -1)
    return (write(2, strerror(errno), strlen(strerror(errno))));
  printf(response.msg);
  if (strncmp(response.reply_code, "430", 3) == 0)
    return (-1);
  return (1);
}

int		identify_user(int socket_fd, char *addr)
{
  t_command	command;
  t_message	response;
  char		buffer[512];

  memset(&command, 0, sizeof(t_command));
  memset(&buffer, 0, 512);
  if (receive_msg(socket_fd, &response) == -1)
    return (write(2, strerror(errno), strlen(strerror(errno))));
  printf("Connected to %s.\n", addr);
  my_putstr(response.msg);
  my_putstr("Username: ");
  read(1, buffer, 512);
  strcpy(command.command_code, CMD_USER);
  strcpy(command.data, buffer);
  send_cmd(socket_fd, &command);
  if (receive_msg(socket_fd, &response) == -1)
    return (write(2, strerror(errno), strlen(strerror(errno))));
  printf(response.msg);
  if (strncmp(response.reply_code, "430", 3) == 0)
    return (-1);
  if (ask_password(socket_fd) == -1)
    return (-1);
  wait_user_command(socket_fd);
  return (0);
}

int			launch_client(char *addr, int port)
{
  int			socket_fd;
  struct sockaddr_in	server_addr;

  if ((socket_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    return (write(2, strerror(errno), strlen(strerror(errno))));
  memset(&server_addr, 0, sizeof(server_addr));
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(port);
  if (inet_pton(AF_INET, addr, &server_addr.sin_addr) != 1)
    return (write(2, "Error: could not use such address", 34));
  if(connect(socket_fd, (struct sockaddr *)&server_addr,
	      sizeof(server_addr)) == -1)
    return (write(2, strerror(errno), strlen(strerror(errno))));
  if (identify_user(socket_fd, addr) == -1)
    {
      shutdown(socket_fd, 2);
      return (-1);
    }
  return (1);
}
