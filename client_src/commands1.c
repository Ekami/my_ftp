/*
** commands1.c for ftp in /home/godard_b/workspace/Projets/En_cours/my_ftp/rendu/client_src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Tue Apr  9 02:46:00 2013 tuatini godard
** Last update Sun Apr 14 00:30:30 2013 tuatini godard
*/

#include "my_ftp.h"
#include "client.h"

int		port_cmd(int socket, char *cmd, char *param)
{
  t_command	t_cmd;
  t_message	response;

  memset(&t_cmd, 0, sizeof(t_command));
  strcpy(t_cmd.command_code, cmd);
  if (param != NULL)
    strcpy(t_cmd.data, param);
  if (send_cmd(socket, &t_cmd) == -1)
    {
      my_putstr("ERROR: Connection to the server failed\n");
      return (-1);
    }
  if (receive_msg(socket, &response) == -1)
    {
      my_putstr("ERROR: Connection to the server failed\n");
      return (-1);
    }
  print_response(&response);
}

int	parse_port(int socket)
{
  char	data[18];
  int	sock1;
  int	sock2;

  srand(time(NULL));
  memset(data, 0, 17);
  strcpy(data, "127,0,0,1,");
  sock1 = rand() % 254 + 1;
  sock2 = rand() % 100 + 1;
  sprintf(data + strlen(data), "%d", sock1);
  strcpy(data + strlen(data), ",");
  sprintf(data + strlen(data), "%d", sock2);
  port_cmd(socket, CMD_PORT, data);
  return (sock1 * 256 + sock2);
}

int	read_files(int connfd, t_message msg)
{
  char	*buffer;
  long	size;
  char	*tmp;

  if (tmp == NULL)
    return (-1);
  tmp = strtok(msg.msg, "=");
  if (tmp == NULL)
    return (-1);
  tmp = strtok(NULL, "\r");
  if (tmp == NULL)
    return (-1);
  size = atol(tmp);
  if ((buffer = malloc(sizeof(char) * size + 1)) == NULL)
    return (-1);
  memset(buffer, 0, size + 1);
  read(connfd, buffer, size);
  my_putstr(buffer);
  return (1);
}

int	finalize_ls(int socket, int connfd, t_message *response)
{
  if (receive_msg(socket, response) == -1)
    {
      my_putstr("ERROR: Connection to the server failed\n");
      return (-1);
    }
  if (print_response(response) == -1)
    return (-1);
  if (read_files(connfd, *response) == -1)
    {
      my_putstr("ERROR: Cannot read file or directory\n");
      return (-1);
    }
  if (receive_msg(socket, response) == -1)
    {
      my_putstr("ERROR: Connection to the server failed\n");
      return (-1);
    }
  if (connfd != -1)
    close(connfd);
  print_response(response);
}

int		ls_cmd(int socket, char *cmd, char *param)
{
  t_command	t_cmd;
  t_message	response;
  int		data_socket;
  int		connfd;

  if ((data_socket = open_data_con(parse_port(socket))) == -1)
    {
      write(2, "ERROR : socket creation : ", 26);
      write(2, strerror(errno), strlen(strerror(errno)));
      return (-1);
    }
  memset(&t_cmd, 0, sizeof(t_command));
  strcpy(t_cmd.command_code, CMD_LIST);
  if (param != NULL)
    strcpy(t_cmd.data, param);
  if (send_cmd(socket, &t_cmd) == -1)
    {
      my_putstr("ERROR: Connection to the server failed\n");
      return (-1);
    }
  connfd = accept(data_socket, (struct sockaddr *) NULL, NULL);
  finalize_ls(socket, connfd, &response);
}
