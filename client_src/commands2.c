/*
** commands2.c for ftp in /home/godard_b/workspace/Projets/En_cours/my_ftp/rendu/client_src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Sat Apr 13 16:39:13 2013 tuatini godard
** Last update Sun Apr 14 02:08:27 2013 tuatini godard
*/

#include "my_ftp.h"
#include "client.h"

int		pwd_cmd(int socket, char *cmd, char *param)
{
  t_command	t_cmd;
  t_message	response;

  memset(&t_cmd, 0, sizeof(t_command));
  strcpy(t_cmd.command_code, CMD_PWD);
  if (send_cmd(socket, &t_cmd) == -1)
    {
      my_putstr("ERROR: Connection to the server failed\n");
      return (-1);
    }
  if (receive_msg(socket, &response) == -1)
    {
      my_putstr("ERROR: Connection to the server failed\n");
      return (-1);
    }
  print_response(&response);
}

int		quit_cmd(int socket, char *cmd, char *param)
{
  t_command	t_cmd;
  t_message	response;

  memset(&t_cmd, 0, sizeof(t_command));
  strcpy(t_cmd.command_code, CMD_QUIT);
  if (send_cmd(socket, &t_cmd) == -1)
    {
      my_putstr("ERROR: Connection to the server failed\n");
      return (-1);
    }
  if (receive_msg(socket, &response) == -1)
    {
      my_putstr("ERROR: Connection to the server failed\n");
      return (-1);
    }
  print_response(&response);
  close(socket);
  exit(0);
}

int		cwd_cmd(int socket, char *cmd, char *param)
{
  t_command	t_cmd;
  t_message	response;

  memset(&t_cmd, 0, sizeof(t_command));
  strcpy(t_cmd.command_code, CMD_CWD);
  if (param != NULL)
    strcpy(t_cmd.data, param);
  if (send_cmd(socket, &t_cmd) == -1)
    {
      my_putstr("ERROR: Connection to the server failed\n");
      return (-1);
    }
  if (receive_msg(socket, &response) == -1)
    {
      my_putstr("ERROR: Connection to the server failed\n");
      return (-1);
    }
  print_response(&response);
}
