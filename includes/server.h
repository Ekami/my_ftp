/*
** server.h for ftp in /home/godard_b/workspace/Projets/En_cours/my_ftp/rendu/server_src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Mon Apr  1 17:27:56 2013 tuatini godard
** Last update Fri Apr 12 21:59:15 2013 tuatini godard
*/

#ifndef			_SERVER_H_
# define		_SERVER_H_
# define MAX_CLIENTS	1000
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <time.h>
#include <grp.h>
#include <pwd.h>
#include "my_ftp.h"

typedef enum
  {
    ASCII_TEXT,
    EBCDIC_TEXT,
    IMAGE_BINARY,
    LOCAL_FORMAT
  } transfert_type;

typedef struct		s_attr
{
  char			initial_dir[1024];
  transfert_type	transf_type;
  char			ft_ip[15];
  int			ft_port;
  int			ft_sock;
}			t_attr;

typedef struct	s_commands_ptr
{
  int		(*exec_cmd)(t_attr *attr, t_command *cmd, int socket);
  char		cmd_code[FTP_CMD_CODE_SIZE];
}		t_command_ptr;

void	append(char *s, char c);
int	start_server(int port);
void	wait_user_inputs(int client_fd);
int	check_username(t_attr *attr, t_command *message, int socket);
int	check_password(t_attr *attr, t_command *message, int socket);
int	syst_cmd(t_attr *attr, t_command *cmd, int socket);
int	pwd_cmd(t_attr *attr, t_command *cmd, int socket);
int	cd_cmd(t_attr *attr, t_command *cmd, int socket);
int	unkown_cmd(t_attr *attr, t_command *cmd, int socket);
int	type_cmd(t_attr *attr, t_command *cmd, int socket);
int	pasv_cmd(t_attr *attr, t_command *cmd, int socket);
int	port_cmd(t_attr *attr, t_command *cmd, int socket);
int	create_data_connection(t_attr *attr, t_message *msg, int text_socket);
int	list_cmd(t_attr *attr, t_command *cmd, int socket);
int	quit_cmd(t_attr *attr, t_command *cmd, int socket);
char	*check_dir(t_attr *attr, char *path);
int	is_special_dir(char *dir);
int	get_file_infos(t_attr *attr, char *filename,
		       char *buffer, struct stat *s_buff);

#endif /* !_SERVER_H */
