/*
** my_ftp.h for ftp in /home/godard_b/workspace/Projets/En_cours/my_ftp/rendu
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Mon Apr  1 16:33:21 2013 tuatini godard
** Last update Sat Apr 13 21:02:45 2013 tuatini godard
*/

#ifndef		_MY_FTP_H_
# define	_MY_FTP_H_
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
# define REPL_110 \
  "110 Restart marker reply.\r\n"
# define REPL_120 \
  "120 Try again in 2 minutes.\r\n"
# define REPL_125 \
  "125 Data connection already open; transfer starting.\r\n"
# define REPL_150 \
  "150 File status okay; total bytes=%ld\r\n"
# define REPL_200 \
  "200 Command okay; about to open data connection.\r\n"
# define REPL_202 \
  "202 Command not implemented, superfluous at this site.\r\n"
# define REPL_211 \
  "221 System status, or system help reply.\r\n"
# define REPL_211_STATUS \
  "221-status of %s.\r\n"
# define REPL_211_END \
  "221 End of status.\r\n"
# define REPL_212 \
  "212 Directory status.\r\n"
# define REPL_213 \
  "213 File status.\r\n"
# define REPL_214 \
  "214 Help message.\r\n"
# define REPL_214_END \
  "214 End Help message.\r\n"
# define REPL_215 \
  "215 UNIX system type.\r\n"
# define REPL_220 \
  "220 Service ready for new user.\r\n"
# define REPL_221 \
  "221 Service closing control connection.\r\n"
# define REPL_225 \
  "225 Data connection open; no transfer in progress.\r\n"
# define REPL_226 \
  "226 Closing data connection.\r\n"
# define REPL_227 \
  "227 Entering Passive Mode (%s,%s,%s,%s,%s,%s).\r\n"
# define REPL_230 \
  "230 User logged in, proceed.\r\n"
# define REPL_250 \
  "250 Requested file action okay, completed.\r\n"
# define REPL_257 \
  "257 %s created.\r\n"
# define REPL_257_PWD \
  "257 \"%s\" is current working dir.\r\n"
# define REPL_331 \
  "331 User OK. Password required.\r\n"
# define REPL_331_ANON \
  "331 Anonymous login okay, send your complete email as your password.\r\n"
# define REPL_332 \
  "332 Need account for login.\r\n"
# define REPL_350 \
  "350 Requested file action pending further information.\r\n"
# define REPL_421 \
  "421 Service not available, closing control connection.\r\n"
# define REPL_425 \
  "425 Can't open data connection.\r\n"
# define REPL_426 \
  "426 Connection closed; transfer aborted.\r\n"
# define REPL_430 \
  "430 Invalid username or password.\r\n"
# define REPL_450 \
  "450 Requested file action not taken.\r\n"
# define REPL_451 \
  "451 Requested action aborted. Local error in processing.\r\n"
# define REPL_452 \
  "452 Requested action not taken.\r\n"
# define REPL_500 \
  "500 Syntax error, command unrecognized.\r\n"
# define REPL_501 \
  "501 Syntax error in parameters or arguments.\r\n"
# define REPL_502 \
  "502 Command not implemented.\r\n"
# define REPL_503 \
  "503 Bad sequence of commands.\r\n"
# define REPL_504 \
  "504 Command not implemented for that parameter.\r\n"
# define REPL_530 \
  "530 Not logged in.\r\n"
# define REPL_532 \
  "532 Need account for storing files.\r\n"
# define REPL_550 \
  "550 Requested action not taken.\r\n"
# define REPL_551 \
  "551 Requested action aborted. Page type unknown.\r\n"
# define REPL_552 \
  "552 Requested file action aborted.\r\n"
# define REPL_553 \
  "553 Requested action not taken.\r\n"
# define CMD_ABOR		"ABOR"
# define CMD_ACCT		"ACCT"
# define CMD_ADAT		"ADAT"
# define CMD_ALLO		"ALLO"
# define CMD_APPE		"APPE"
# define CMD_AUTH		"AUTH"
# define CMD_CCC		"CCC"
# define CMD_CDUP		"CDUP"
# define CMD_CONF		"CONF"
# define CMD_CWD		"CWD"
# define CMD_DELE		"DELE"
# define CMD_ENC		"ENC"
# define CMD_EPRT		"EPRT"
# define CMD_EPSV		"EPSV"
# define CMD_FEAT		"FEAT"
# define CMD_HELP		"HELP"
# define CMD_LANG		"LANG"
# define CMD_LIST		"LIST"
# define CMD_LPRT		"LPRT"
# define CMD_LPSV		"LPSV"
# define CMD_MDTM		"MDTM"
# define CMD_MIC		"MIC"
# define CMD_MKD		"MKD"
# define CMD_MLSD		"MLSD"
# define CMD_MLST		"MLST"
# define CMD_MODE		"MODE"
# define CMD_NLST		"NLST"
# define CMD_NOOP		"NOOP"
# define CMD_OPTS		"OPTS"
# define CMD_PASS		"PASS"
# define CMD_PASV		"PASV"
# define CMD_PBSZ		"PBSZ"
# define CMD_PORT		"PORT"
# define CMD_PROT		"PROT"
# define CMD_PWD		"PWD"
# define CMD_QUIT		"QUIT"
# define CMD_REIN		"REIN"
# define CMD_REST		"REST"
# define CMD_RETR		"RETR"
# define CMD_RMD		"RMD"
# define CMD_RNFR		"RNFR"
# define CMD_RNTO		"RNTO"
# define CMD_SITE		"SITE"
# define CMD_SIZE		"SIZE"
# define CMD_SMNT		"SMNT"
# define CMD_STAT		"STAT"
# define CMD_STOR		"STOR"
# define CMD_STOU		"STOU"
# define CMD_STRU		"STRU"
# define CMD_SYST		"SYST"
# define CMD_TYPE		"TYPE"
# define CMD_USER		"USER"
# define CMD_XCUP		"XCUP"
# define CMD_XMKD		"XMKD"
# define CMD_XPWD		"XPWD"
# define CMD_XRCP		"XRCP"
# define CMD_XRMD		"XRMD"
# define CMD_XRSQ		"XRSQ"
# define CMD_XSEM		"XSEM"
# define CMD_XSEN		"XSEN"
# define FTP_MSG_SIZE		1024
# define FTP_REPL_CODE_SIZE	4
# define FTP_T_MSG_SIZE		(FTP_MSG_SIZE + FTP_REPL_CODE_SIZE)
# define FTP_CMD_DATA_SIZE	512
# define FTP_CMD_CODE_SIZE	5
# define FTP_T_CMD_SIZE		(FTP_CMD_DATA_SIZE + FTP_CMD_CODE_SIZE)

typedef struct	s_message
{
  char		reply_code[FTP_REPL_CODE_SIZE + 1];
  char		msg[FTP_MSG_SIZE + 1];
}		t_message;

typedef struct	s_command
{
  char		command_code[FTP_CMD_CODE_SIZE + 1];
  char		data[FTP_CMD_DATA_SIZE + 1];
}		t_command;

int	my_putchar(char c);
int	my_putstr(char *str);
int	send_msg(int socket, t_message *query);
int	receive_msg(int a_socket, t_message *ap_response);
int	cmp_codes(char *code1, char *code2, int size);
void	set_message_reply(t_message *t_msg, char *reply_code);
void	set_msg_reply_code(t_message *msg, char *code);
void	set_msg(t_message *t_msg, char *msg);
int	receive_cmd(int a_socket, t_command *ap_response);
int	send_cmd(int socket, t_command *query);
int	open_data_con(int port);

#endif /* !_MY_FTP_H_ */
