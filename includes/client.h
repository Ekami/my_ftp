/*
** client.h for ftp in /home/godard_b/workspace/Projets/En_cours/my_ftp/rendu/client_src
** 
** Made by tuatini godard
** Login   <godard_b@epitech.net>
** 
** Started on  Mon Apr  1 20:41:52 2013 tuatini godard
** Last update Sun Apr 14 01:47:20 2013 tuatini godard
*/

#ifndef		_CLIENT_H_
# define	_CLIENT_H_
#include "my_ftp.h"

int	launch_client(char *addr, int port);
int	port_cmd(int socket, char *cmd, char *param);
int	ls_cmd(int socket, char *cmd, char *param);
int	pwd_cmd(int socket, char *cmd, char *param);
int	quit_cmd(int socket, char *cmd, char *param);
int	cwd_cmd(int socket, char *cmd, char *param);
int	print_response(t_message *response);

typedef struct	s_command_ptr
{
  int		(*exec_cmd)(int socket, char *cmd, char *param);
  char		cmd[FTP_MSG_SIZE];
}		t_command_ptr;

#endif /* !_CLIENT_H_ */
